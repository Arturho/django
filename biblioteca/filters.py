from django_filters import rest_framework as filters
from django.contrib.auth.models import User
from .serializers import UsersSerializer

class UserFilter(filters.FilterSet):
    #min_price = filters.NumberFilter(field_name="price", lookup_expr='gte')

    class Meta:
        model = User
        fields = ['is_active', 'username', 'email']
   

