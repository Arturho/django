from rest_framework import status
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView,CreateAPIView
from rest_framework.decorators import action
from rest_framework.response import Response
from django.utils.translation import gettext as _
from rest_framework.permissions import IsAuthenticated
from .serializers import UsersSerializer
from .filters import UserFilter
from django_filters import rest_framework as filters

from django.contrib.auth.models import User
from django.http import HttpResponse
from .models.profile import Profile
from .models.book import Book
from .models.author import Author


class UserView(ListAPIView):
    print("HolA")
    queryset = User.objects.all()
    serializer_class = UsersSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter

class UserDetailView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UsersSerializer

class UserStoreView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UsersSerializer

class LogoutView(APIView):
    @action(detail=False, methods=['post'])
    def post(self, request):
        request.user.auth_token.delete()
        response = {
            "message": _("Successfully logged out."),
            "code":status.HTTP_200_OK
        }

        return Response(response)







