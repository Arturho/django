from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework import validators
from .models.profile import Profile


class ProfilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['address', 'avatar']
        
class UsersSerializer(serializers.ModelSerializer):
    profile = ProfilesSerializer()
    mi_data = serializers.SerializerMethodField()

    def get_mi_data(self, obj):
        if obj.is_active == True:
            return 'Esta activo'
        return 'Usurio inactivo'

    #def post(self, validate_data):
    #    print('hola')
    #   return User(**validate_data)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'profile', 'mi_data']

    
