from django.contrib import admin
from biblioteca.models.profile import Profile
from biblioteca.models.book import Book
from biblioteca.models.author import Author
# Register your models here.
admin.site.register(Profile)
admin.site.register(Book)
admin.site.register(Author)