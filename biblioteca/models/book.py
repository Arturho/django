from django.db import models
from biblioteca.models.author import Author
from django.db.models.signals import post_save
from django.dispatch import receiver

class Book(models.Model):
    status_book= (('P', 'Publish'),('M', 'MEAP'))
    
    isbn = models.CharField(max_length=13, primary_key=True)
    tittle = models.CharField(max_length=70, blank=True)
    pages = models.PositiveIntegerField()
    date_publish = models.DateField(null=True)
    image = models.URLField(max_length=85, null=True)
    desc_short = models.CharField(max_length=2000, default='Sin reseña')
    status = models.CharField(max_length=1, choices=status_book)
    category = models.CharField(max_length=50)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name="books")

    def __str__(self):
        return self.tittle