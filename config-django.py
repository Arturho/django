'''
    Pasos para configurar un proyecto en django

'''
1. sudo apt update  #actualizar paquetes
2. python3 -V       #ver la version de python instalada
3. sudo apt install python3-django  #instalar django
4. django-admin --version   #version de django
5. sudo apt install python3-pip #instalacion de pip
6. sudo apt install python3-venv #instalacion de venv
7. mkdir ~/django-app   #crear nuevo pproyecto
8. cd ~/django-app  #moverse al directorio
9. python3.6 -m venv myenv  #crear un entorno virtual
10.source myenv/bin/activate #activar el entorno virtual 
11.pip install django #instalar django
12.django-admin --version
13.deactivate #desactivar el entorno virtual
14. django-admin startproject djangoproject . #inicar un nuevo proyecto
15. python manage.py migrate #migrar base de datos de la aplicacion
16. python manage.py createsuperuser #crear usuario administrativo
17. nano ~/[django-nombre-proyecto]/djangoproject/settings.py
18. ALLOWED_HOST = [IP_SERVER]#editar ips para asociar al proyecto
19. sudo ufw allow 8080#comprobar y abrir el puerto 8080
20. python manage.py runserver IP_SERVER:8080#iniciar servidor
21. python manage.py runserver IP_SERVER:8080#para ingresar al admin

22.python manage.py startapp //iniciar una aplicacion

python manage.py migrate

python manage.py makemigrations polls

python manage.py sqlmigrate polls 0001

python manage.py migrate


python manage.py shell

##Instalacion de la version de desarrollo con GIT
sudo apt update
python3 -V
sudo apt install python3-pip #instalacion de pip
sudo apt install python3-venv #instalacion de venv
git clone git://github.com/django/django ~/django-dev
cd /django-dev
python3.6 -m venv myenv 
source myenv/bin/activate
pip install -e ~/django-dev
django-admin --version



desinntalar mysql
sudo apt-get --purge remove mysql-client mysql-server mysql-common
sudo apt-get autoremov
$ sudo rm -rf /etc/mysql/

